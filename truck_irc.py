#! /usr/bin/env python3

import pypeul
import datetime
import pytz
import threading
import time
import random
import nvidia

class TrucksBot(pypeul.IRC):
  trashtalk = [ "You're talking to me {}!?",
                "C'est ta mère le camioneur {}",
                "{} talks to a bot, this is the end…",
                "I can hl you too {}!",
                "Sorry {}, I don't have much to say." ]

  def on_ready(self):
    self.join("#nvinterns")

  def on_self_join(self, target):
    threading.Thread(target=self.thread, args=(target,)).start()
  
  def on_channel_message(self, umask, target, msg):
    if self.myself.nick in msg:
      self.message(target, random.choice(self.trashtalk).format(umask.user.nick))
    elif msg.lower().startswith("!menu"):
      self.send_trucks(umask.user.nick, target, msg)

  def thread(self, target):
    h, m = 20, 0
    while True:
      t = datetime.datetime.today()
      future = datetime.datetime(t.year,t.month,t.day,h, m)
      if t.hour >= h or (t.hour == h and t.minutes > m):
        future += datetime.timedelta(days=1)
      while future.weekday() > 4:
        future += datetime.timedelta(days=1)
      time.sleep((future-t).seconds)
      self.message(target, "Menu report:")
      self.send_trucks("Trucker", target, "")
      time.sleep(10)

  def send_trucks(self, nick, target, msg):
    other_day = False
    if len(msg.split()) == 2:
      other_day = True
      trucks = get_trucks(today=msg.split()[1])
    else:
      trucks = get_trucks()
    if len(trucks) == 0:
      if nick == "clippix":
        self.message(target, "chut clippix.")
      elif other_day:
        self.message(target, "Couldn't find the menu for that day :(")
      else:
        self.message(target, "Couldn't find the menu for today :(")
    else:
      for comp, locations in trucks.items():
        for k, v in locations.items():
          if "Senor Sisig" in v:
            self.message(target, "BRACE YOURSELVES, SEÑOR SISIG IS COMING!")
          self.message(target, "{} ({}):\n  {}".format(comp, k, "\n  ".join(v)))

def get_trucks(today=None):
  res = {}
  if today:
    today = today.replace("/", ".")
  if today == "tomorrow":
    day = datetime.datetime.now(pytz.timezone("America/Los_Angeles"))
    day += datetime.timedelta(days=1)
    today = day.strftime("%m.%d")
  menu = nvidia.get_menus(today=today)
  if menu:
    res["NVIDIA"] = menu
  return res

if __name__ == "__main__":
  bot = TrucksBot()
  bot.connect('irc.rezosup.org', 6667)
  bot.ident('Trucker')
  bot.set_reconnect(lambda x: x * 5)
  bot.run()

#! /usr/bin/env python3

from bs4 import BeautifulSoup
import urllib.request
import requests
import datetime
import calendar
import pytz
from unidecode import unidecode
import re

def unformat(s):
    s = re.sub("\s\s+", ", ", s)
    s = s.replace("\n", " ")
    return s

def get_menu(cafe_id, today):
    tz = pytz.timezone("America/Los_Angeles")
    if not today:
        today = datetime.datetime.now(tz)
    else:
        today = datetime.datetime.strptime(today + ".2016 12:00", "%m.%d.%Y %H:%M")
        today = today.replace(tzinfo=tz)

    # Get menuid
    r = requests.get("http://www.aramarkcafe.com/layouts/canary_2010/locationhome.aspx",
            params={"locationid": cafe_id})
    bs = BeautifulSoup(r.content.decode("utf-8"), "html5lib")
    weekcode = 0
    for elt in bs.find(id="sideBar_pnlWeeklyMenu").find('a')["href"].split('&'):
        if "menuid" in elt:
            weekcode = int(elt[len("menuid="):])

    dayid = today.isoweekday() + 1

    r = requests.get("http://www.aramarkcafe.com/layouts/canary_2010/locationhome.aspx",
        params={"locationid": cafe_id, "pageid": 20, "menuid": weekcode, "dayid": dayid})

    bs = BeautifulSoup(r.content.decode("utf-8"), "html5lib")

    res = []
    for dish in bs.find_all(class_="item"):
        txt = unidecode(dish.parent.previous_sibling.previous_sibling.find("h6").get_text().strip())
        txt = "[{}] ".format(unformat(txt).replace(".", ""))
        txt += unidecode(dish.find(class_="name").get_text().strip())
        if txt == "":
             continue
        txt = unformat(txt)
        desc = unidecode(dish.find(class_="description").get_text().strip())
        if desc != "":
            desc = unformat(desc)
            txt += " ({})".format(desc)
        res.append(txt)

    return res

def get_menus(today=None):
    res = {}
    menu = get_menu(1242, today)
    if menu:
        res["Nth Street Cafe"] = menu
    menu = get_menu(4138, today)
    if menu:
        res["Building I"] = menu
    return res

if __name__ == "__main__":
    print(get_menus())

#! /usr/bin/env python3

import pypeul

class PingBot(pypeul.IRC):
  def on_ready(self):
    self.join("#arista")
  def on_self_join(self, target):
    self.message(target, "!trucks")
    self.enabled = False

if __name__ == "__main__":
  bot = PingBot()
  bot.connect('irc.rezosup.org', 6667)
  bot.ident('TruckPinger')
  bot.run()

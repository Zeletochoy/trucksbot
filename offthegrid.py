#! /usr/bin/env python3

from bs4 import BeautifulSoup
import urllib.request
import requests
import datetime
import pytz

def get_trucks_at(market_id, today, offset=0):
    r = requests.get("http://offthegridsf.com/wp-admin/admin-ajax.php",
        params={"action": "otg_market", "market": market_id, "delta": offset})
    bs = BeautifulSoup(r.content.decode("utf-8"))
    if not today:
      today = datetime.datetime.now(pytz.timezone("America/Los_Angeles")).strftime("%m.%d")
    idx, i = -1, -1

    for div in bs.find_all("div"):
        if "otg-market-data-events-pagination" in div["class"]:
            i += 1
            if today == div.get_text().strip():
                idx = i
                break
    
    if idx < 0:
      if offset >= 2:
        return None
      else:
        return get_trucks_at(market_id, today, offset + 1)

    i = 0
    for div in bs.find_all("div"):
        if "otg-market-data-vendors" in div["class"]:
            if i == idx:
                return [a.get_text() for a in div.find_all("div")[1].find_all("a")]
            i += 1

def get_trucks(today=None):
    res = {}
    trucks = get_trucks_at(5, today)
    if trucks:
        res["UN Plaza"] = trucks
    trucks = get_trucks_at(10, today)
    if trucks:
        res["Civic Center"] = trucks
    return res

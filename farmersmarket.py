#! /usr/bin/env python3

import datetime
import pytz

farmerstrucks = [
  "The Crepe Escape",
  "Copper Top Ovens",
  "Estrellitas",
  "Natures Taste",
  "Roliroti",
  "All Star Tamales",
  "Ultimate Souvlaki",
  "Ko's Kitchen",
  "Sukhi's Gourmet",
  "The Hummus Guy",
  "Heidi's Pies" ]

def get_trucks(today=None):
  day = datetime.datetime.now(pytz.timezone("America/Los_Angeles"))
  if today:
    today = today.replace("/", ".")
    try:
      m, d = map(int, today.split("."))
      day = day.replace(month=m, day=d)
    except:
      return {}
  if day.weekday() == 2:
    return {"UN Plaza": farmerstrucks}
  return {}
